Feeel is a hobby project that I'm working on in my spare time. As such, any further development relies on the work of enthusiastic volunteers wanting to make the project a bit better.

Here's how you can get involved:

Develop
=======
Find a development issue on [Gitlab](https://gitlab.com/enjoyingfoss/feeel/-/issues) and start working on it.

Need help getting around the code? Get help from the community on [Matrix](https://matrix.to/#/!jFShhgWHRXehKXrToU:matrix.org?via=matrix.org).

Submit exercise photos
======
Exercise photos are sorely needed. See [this issue](https://gitlab.com/enjoyingfoss/feeel/-/issues/10) for a list.

The photos you submit must be your own work (copyright belongs only to you).

Process exercise photos
======
You can take already contributed or CC0, CC BY, or CC BY-SA photos and turn them into low-poly versions using the [FOSStriangulator app](https://github.com/FOSStriangulator/FOSStriangulator).

See [this tutorial](https://gitlab.com/enjoyingfoss/feeel/-/wikis/Processing-photos) for a guide on how to triangulate.

Translate
=========
You can help translate through [Weblate](https://hosted.weblate.org/projects/feeel/strings/).

Spread the word
===============
Have a social media account? Or a blog? Give Feeel a mention!

Rate and review the app
=======================
Here is a [list of sites where you can rate or review Feeel](https://gitlab.com/enjoyingfoss/feeel/-/wikis/App-rating-and-reviews).

Other ways to help
===============

ANYONE can contribute, no special skills required. Message me on the [Feeel channel on Matrix](https://matrix.to/#/!jFShhgWHRXehKXrToU:matrix.org?via=matrix.org) if you'd like to help.