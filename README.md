Feeel
=====

Feeel is an open-source workout app for doing simple at-home exercises.

This is a rewrite of the [original app](https://gitlab.com/enjoyingfoss/feeel-legacy) in Flutter, to make development easier and allow for an iOS version. Still free as in freedom.

Contribute
====

ANYONE can contribute:
- If you have a phone with a camera (or an actual camera), you can **contribute exercise photos**. (These don't need to look particularly good. Even lower resolution living room shots are fine.)
- If you have a computer, you can **turn exercise photos into low-poly versions** using the FOSStriangulator app.
- If you know a different language, you can **translate**.
- UX designers can help **design UX** for new features.
- UI designers can help with **visuals and animations**

See CONTRIBUTING.md for more details.

Donate
=======
Support the project via **[Liberapay](https://liberapay.com/Feeel/)**. Every bit helps. The more money is donated, the more time I can afford to work on this project.

Get in touch
=======
You can chat with the community on **[Matrix](https://matrix.to/#/!jFShhgWHRXehKXrToU:matrix.org?via=matrix.org)**.

Install
====
[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/com.enjoyingfoss.feeel/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.enjoyingfoss.feeel)
